﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExcelReport;

namespace ExcelUtility.Base
{
    /// <summary>
    /// EXCEL模板指定区域表格数据格式化器创建者类（独立使用）
    /// 作者：Zuowenjun
    /// 日期：2016-1-7
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TableFormatterBuilder<T> : FormatterBuilder<Func<T, object>>
    {
        protected IEnumerable<T> dataSource = null;
        protected string startParamName = null;
        protected SheetParameterContainer paramContainer = null;
        protected List<FormatterBuilder<T, Func<T, object>>> otherFormatterBuilders;
        public TableFormatterBuilder(IEnumerable<T> dataSource, string startParamName)
        {
            this.dataSource = dataSource;
            this.startParamName = startParamName;
            this.otherFormatterBuilders = new List<FormatterBuilder<T, Func<T, object>>>();
        }

        public void AppendFormatterBuilder(params FormatterBuilder<T, Func<T, object>>[] formatterBuilders)
        {
            otherFormatterBuilders.AddRange(formatterBuilders.AsEnumerable());
        }

        internal override IEnumerable<ElementFormatter> GetFormatters(SheetParameterContainer paramContainer)
        {
            this.paramContainer = paramContainer;
            List<EmbeddedFormatter<T>> cellFormatters = new List<EmbeddedFormatter<T>>();
            foreach (var kv in formatters)
            {
                cellFormatters.Add(new CellFormatter<T>(paramContainer[kv.Key], kv.Value as Func<T, object>));
            }
            if (otherFormatterBuilders.Count > 0)
            {
                foreach (var item in otherFormatterBuilders)
                {
                    cellFormatters.AddRange(item.GetFormatters(paramContainer));
                }
            }
            return new[] { CreateElementFormatter(paramContainer[startParamName], cellFormatters.ToArray()) };
        }

        protected virtual ElementFormatter CreateElementFormatter(Parameter param, params EmbeddedFormatter<T>[] cellFormatters)
        {
            return new TableFormatter<T>(param, dataSource, cellFormatters);
        }

        protected override ElementFormatter CreateElementFormatter(Parameter param, Func<T, object> value)
        {
            throw new NotImplementedException();
        }
    }
}
